# AbDf1

## Contents
- Description
- Dependencies
- Release Notes

## Description
Allen Bradley DF1 Serial Protocol EPICS Device Support 
or PLC-2, PLC-5, and PLC-5/V Controllers.

## Dependencies
- drvSerial

## Release Notes
- See git commit logs for now until a formal release notes doc is created